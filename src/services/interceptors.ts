import axios from 'axios';

export default function setup(token: any) {
    axios.interceptors.request.use(function(config: any) {
        if(token) {
            config.headers["x-access-token"] = `${token}`;
        }
        return config;
    }, function(err) {
        return Promise.reject(err);
    });
}