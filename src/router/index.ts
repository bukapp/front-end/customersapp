import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import ProfileView from '../views/ProfileView.vue'
import LoginView from '../views/LoginView.vue'
import RestaurantView from '../views/RestaurantView.vue'
import HomeRestaurantView from '../views/HomeRestaurantView.vue'
import ComposeMenuView from '../views/ComposeMenuView.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/profile',
    name: 'profile',
    component: ProfileView
  },
  {
    path: '/home-restaurant',
    name: 'home-restaurant',
    component: HomeRestaurantView
  },
  {
    path: '/login',
    name: 'login',
    component: LoginView
  },
  {
    path: '/restaurant',
    name: 'restaurant',
    component: RestaurantView
  },
  {
    path: '/menu',
    name: 'menu',
    component: ComposeMenuView
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
