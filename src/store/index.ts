import Vue from 'vue'
import Vuex from 'vuex'
import restaurant from './modules/restaurant'
import food from './modules/food'
import user from './modules/user'
import cart from './modules/cart'
import payment from './modules/payment'
import command from './modules/command'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    restaurant,
    food,
    user,
    cart,
    payment,
    command
  }
})