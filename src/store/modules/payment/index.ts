
import router from '@/router';
import axios from 'axios';

const state = {
    //Cart: {},
}

const mutations = {
   /* UPDATE_CART(state: any, payload: any) {
        state.Cart = payload;
    },*/
}


const actions = {
    getPayment({ commit }: any, amount: any) {
        axios.get(`/gateway/payments/payment?amount=${amount}`).then((response) => {
            console.log(response)
            //commit('UPDATE_CART', response.data)
        });
    },
    addPayment({ commit }: any, payment: any) {
        axios.post('/gateway/payments/', payment).then((response) => {
            // redirect to payment page
        });
    }
   /* putCart({ commit }: any, cart: any) {
        axios.put(`/gateway/carts/`, cart).then((response) => {
            commit('UPDATE_CART', response.data)
            router.push('restaurant')
        });
    },*/
}

const getters = {
   // cart: (state: any) => state.cart,
}

const paymentModule = {
    state,
    mutations,
    actions,
    getters
}

export default paymentModule;