
import router from '@/router';
import axios from 'axios';

const state = {
    Cart: {},
    alterUser: false,
}

const mutations = {
    UPDATE_CART(state: any, payload: any) {
        state.Cart = payload;
    },
    UPDATE_ALERT(state: any, payload: any) {
        state.alertCart = payload;
    }

}


const actions = {
    getCart({ commit }: any) {
        axios.get(`/gateway/carts/`).then((response) => {
            commit('UPDATE_CART', response.data)
        });
    },
    putCart({ commit }: any, cart: any) {
        axios.put(`/gateway/carts/`, cart).then((response) => {
            commit('UPDATE_CART', response.data)
            commit('UPDATE_ALERT', true)
            // router.push('restaurant')
        });
    },
    updateCartAlert({ commit }: any, alertState: boolean) {
        commit('UPDATE_ALERT', alertState);
      }
}

const getters = {
    cart: (state: any) => state.cart,
}

const cartModule = {
    state,
    mutations,
    actions,
    getters
}

export default cartModule;