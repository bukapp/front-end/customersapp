
import axios from 'axios';

const state = {
  RestaurantsList: [],
  SelectedRestaurant: {},
}

const mutations = {
  UPDATE_RESTAURANTS_LIST(state: any, payload: any) {
    state.RestaurantsList = payload;
  },
  UPDATE_SELECTED_RESTAURANT(state: any, payload: any) {
    state.SelectedRestaurant = payload;
  }
}


const actions = {
  getRestaurantsList({ commit }: any) {
    axios.get(`/gateway/restaurants/`).then((response) => {
      commit('UPDATE_RESTAURANTS_LIST', response.data)
    });
  },
  addRestaurant({ commit }: any, restaurant: any) {
    axios.post('/gateway/restaurants/', restaurant).then((response) => {
      commit('UPDATE_RESTAURANTS_LIST', response.data)
    });
  },
  selectRestaurant({ commit }: any, restaurant: any) { 
    commit('UPDATE_SELECTED_RESTAURANT', restaurant)
  },
}

const getters = {
  restaurants: (state: any) => state.RestaurantsList,
  restaurant: (state: any) => state.SelectedRestaurant,
  restaurantById: (state: any) => (id: any) => {
    return state.RestaurantsList.find((RestaurantsList: { id: any; }) => RestaurantsList.id === id)
  }
}

const restaurantModule = {
  state,
  mutations,
  actions,
  getters
}

export default restaurantModule;