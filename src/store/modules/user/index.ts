
import router from '@/router';
import axios from 'axios';
import jwt_decode from "jwt-decode";
import interceptorsSetup from '../../../services/interceptors'

const state = {
  user: {},
  isConnected: false,
  token: {},
  response: {},
  alertUser: false
}

const mutations = {
  UPDATE_USER(state: any, payload: any) {
    state.user = payload;
  },
  UPDATE_TOKEN(state: any, payload: any) {
    state.token = payload;
  },
  UPDATE_RESPONSE(state: any, payload: any) {
    state.response = payload;
  },
  UPDATE_ISCONNECTED(state: any, payload: any) {
    state.isConnected = payload;
  },
  UPDATE_ALERT(state: any, payload: any) {
    state.alertUser = payload;
  }
}

const actions = {
  loginUser({ commit }: any, user: any) {
    axios.post(`/gateway/users/customers/login`, user).then((response) => {
      const token = response.data
      const decoded = jwt_decode(token);

      commit('UPDATE_TOKEN', response.data)
      commit('UPDATE_USER', decoded)
      commit('UPDATE_ISCONNECTED', true)
      
      localStorage.user = JSON.stringify(decoded)
      localStorage.token = token
      localStorage.isConnected = true

      interceptorsSetup(token)

      router.push('/')
    });
  },
  getUser({ commit }: any) {
    axios.get(`/gateway/users/customers/`).then((response) => {
      commit('GET_PROFILE', response.data)
    });
  },
  addUser({ commit }: any, user: any) {
    axios.post('/gateway/users/customers/', user).then((response) => {
      commit('UPDATE_RESPONSE', response.data)
      commit('UPDATE_ALERT', true)
      router.push('/')
    });
  },
  updateUser({ commit }: any, user: any) {
    axios.put('/gateway/users/customers/', user).then((response) => {
      console.log(user)
      commit('UPDATE_USER', response.data)
      alert("Compte mis à jour")
    });
  },
  updateUserAlert({ commit }: any, alertState: boolean) {
    commit('UPDATE_ALERT', alertState);
  }
}

const getters = {
  user: (state: any) => state.user,
  token: (state: any) => state.token,
  isConnected: (state: any) => state.isConnected,
}

const userModule = {
  state,
  mutations,
  actions,
  getters
}

export default userModule;