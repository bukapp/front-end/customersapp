import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import mdiVue from 'mdi-vue/v2'
import * as mdijs from '@mdi/js'

import interceptorsSetup from './services/interceptors'

if(localStorage.token){
  interceptorsSetup(localStorage.token)
}

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(mdiVue, {
  icons: mdijs
}) 

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
