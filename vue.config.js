const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/gateway': {
        target: 'http://localhost:3000/',
        changeOrigin: true,
        pathRewrite: {
          '^/gateway': ''
        }
      }
    }
  }
})
